// import 'package:flutter/material.dart';
//
// void main() {
//   runApp(const MyApp());
// }
//
// class MyApp extends StatelessWidget {
//   const MyApp({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter Demo',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//       ),
//       home: const MyHomePage(title: 'Home Page'),
//     );
//   }
// }
//
// class MyHomePage extends StatefulWidget {
//   const MyHomePage({Key? key, required this.title}) : super(key: key);
//   final String title;
//
//   @override
//   State<MyHomePage> createState() => _MyHomePageState();
// }
//
// class _MyHomePageState extends State<MyHomePage> {
//   int _counter = 0;
//
//   void _incrementCounter() {
//     setState(() {
//       _counter++;
//     });
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text(widget.title),
//       ),
//       body: Center(
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: <Widget>[
//             const Text(
//               'You have pushed the button this many times:',
//             ),
//             Text(
//               '$_counter',
//               style: Theme.of(context).textTheme.headline4,
//             ),
//           ],
//         ),
//       ),
//       floatingActionButton: FloatingActionButton(
//         onPressed: _incrementCounter,
//         tooltip: 'Increment',
//         child: const Icon(Icons.add),
//       ),
//     );
//   }
// }

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

// Main Application
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Example',
      // Routes
      routes: <String, WidgetBuilder>{
        '/': (_) => const Login(), // Login Page
        '/home': (_) => const Home(), // Home Page
        '/signUp': (_) => const SignUp(), // The SignUp page
        '/forgotPassword': (_) => const ForgotPwd(), // Forgot Password Page
        '/screen1': (_) =>
            const Screen1(), // Any View to be navigated from home
      },
    );
  }
}

// The login page
class Login extends StatelessWidget {
  const Login({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text("Login Page"),

            // The button on pressed, logs-in the user to and shows Home Page
            FlatButton(
                onPressed: () =>
                    Navigator.of(context).pushReplacementNamed("/home"),
                child: const Text("Login")),

            // Takes user to sign up page
            FlatButton(
                onPressed: () => Navigator.of(context).pushNamed("/signUp"),
                child: const Text("SignUp")),

            // Takes user to forgot password page
            FlatButton(
                onPressed: () =>
                    Navigator.of(context).pushNamed("/forgotPassword"),
                child: const Text("Forgot Password")),
          ],
        ),
      ),
    );
  }
}

// Home page
class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text("Home Page"),

            // Logs out user instantly from home
            FlatButton(
                onPressed: () =>
                    Navigator.of(context).pushReplacementNamed("/"),
                child: const Text("Logout")),

            // Takes user to Screen1
            FlatButton(
                onPressed: () => Navigator.of(context).pushNamed("/screen1"),
                child: const Text("Screen 1")),
          ],
        ),
      ),
    );
  }
}

// Sign Up Page
class SignUp extends StatelessWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text("Sign Up Page"),

            // To make an api call with SignUp data and take back user to Login Page
            FlatButton(
                onPressed: () {
                  //api call to sign up the user or whatever
                  Navigator.of(context).pop();
                },
                child: const Text("SignUp")),
          ],
        ),
      ),
    );
  }
}

// Forgot Password page
class ForgotPwd extends StatelessWidget {
  const ForgotPwd({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text("Sign Up"),

            // Make api call to resend password and take user back to Login Page
            FlatButton(
                onPressed: () {
                  //api call to reset password or whatever
                  Navigator.of(context).pop();
                },
                child: const Text("Resend Passcode")),
          ],
        ),
      ),
    );
  }
}

// Any Screen
class Screen1 extends StatelessWidget {
  const Screen1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text("Screen 1"),

            // Takes the user to the view from which the user had navigated to this view
            FlatButton(
                onPressed: () => Navigator.of(context).pop(),
                child: const Text("Back")),

            // Takes back the user to Home page and Logs out the user
            FlatButton(
                onPressed: () async {
                  Navigator.of(context).popUntil(ModalRoute.withName(
                      "/home")); // Use pop Until if you want to reset all routes until now and completely logout user
                  Navigator.of(context).pushReplacementNamed("/");
                  // Just to show login page and resume back after login
                  // Navigator.of(context).pushNamed('/Login');
                  // On login page after successful login Navigator.of(context).pop();
                  // the app will resume with its last route.
                },
                child: const Text("Logout")),
          ],
        ),
      ),
    );
  }
}
